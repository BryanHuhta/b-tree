#ifndef _HEADER_H
#define _HEADER_H

#include <cstddef>
#include <sstream>
#include <string>
#include <vector>

#include "global.h"
#include "globallib.hpp"

class Header
{
private:
    size_t size_;
    NodeType type_;
    size_t root_offset_;
    size_t list_offset_;
    size_t free_offset_;

public:
    Header();
    
    size_t GetSize() const;
    size_t GetRootOffset()const;
    size_t GetListOffset() const;
    size_t GetFreeOffset() const;

    void SetSize(const size_t&);
    void SetRootOffset(const size_t&);
    void SetListOffset(const size_t&);
    void SetFreeOffset(const size_t&);

    std::string ToString() const;
    void FromString(const std::string&);
    std::string Print() const;
};

#endif
