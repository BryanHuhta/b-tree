cc				= g++ -std=c++0x
flags			= -Werror -Wall -Wextra -I'./include'

include_path	= include/
src_path		= src/
depend			= $(include_path)globallib.hpp

all : main

main : main.o BManager.o Header.o
	$(cc) $(flags) -o $@ $^ 

main.o : $(src_path)main.cpp $(include_path)BManager.h
	$(cc) $(flags) -c $<

BManager.o : $(src_path)BManager.cpp $(include_path)BManager.h
	$(cc) $(flags) -c $<

Header.o : $(src_path)Header.cpp $(include_path)Header.h $(include_path)globallib.hpp
	$(cc) $(flags) -c $<

clean :
	rm -rf *.o
	rm -rf main

