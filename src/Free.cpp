#include "Free.h"

// CONSTRUCTOR / DESTRUCTOR

Free::Free()
{
    Block block;
    free_list_.push_back(block);
}

Free::Free(const std::string& first_offset)
{
    // Parse first offset and build the linked list.
}

// END CONSTRUCTOR / DESTRUCTOR

// METHODS

size_t Free::FindFreeBlock(const size_t& bytes)
{
    size_t offset;

    // Find first available slot in the free list.
    // The block must be either:
    //  1. Exactly the same size as the size of bytes to insert.
    //  2. At least MIN_BYTES larger than the free spot.
    for (index_ = free_list_.begin(); index_ != free_list_.end(); ++index_)
    {
        if (((*index_).b_size >= (bytes + MIN_BYTES)) || 
                ((*index_).b_size == bytes))
        {
            offset = (*index_).b_offset;
        }
    }

    Reset(index_);

    return offset;
}

void Free::UseFreeBlock(const size_t& offset, const size_t& bytes)
{
    iter previous;
    Reset(previous);

    // Find the correct block.
    while (index_ != free_list_.end())
    {
        if ((*index_).b_offset == offset)
        {
            break;
        }
        ++index_;
    }

    // Check if we should delete the block entirely, or simply resize.
    bool resize = (*index_).b_size == bytes;
    
    // Find the previous block, regardless of delete or resize,
    // we need the previous block.
    while (previous != free_list_.end())
    {
        if ((*previous).b_offset == (*index).b_previous)
        {
            break;
        }
        ++previous;
    }

    // If we need to delete, we need to find the next node as well
    // so we can maintain the linked list.
    if (!resize)
    {
        iter next;
        Reset(next);

        while (next != free_list_.end())
        {
            if ((*next).b_offset == (*index).b_next)
            {
                break;
            }
            ++next;
        }

        // Connect the link.
        (*previous).b_next = (*next).b_offset;
        (*next).b_previous = (*previous).b_offset;

        // Delete the block.
        free_list_.erase(index_);
    }
    else
    {
        // Otherwise we just resize.
        (*index_).b_offset -= bytes;
        (*previous).b_next = (*index_).b_offset;
    }

    Reset(index_);
}

std::string Free::ToString() const
{
    // Create a string of information to print to the file.
}

void Free::FromString(const std::string& block)
{
    // Parse a string from disk into a block in memory. Add this block into
    // the free list.
}

// END METHODS
