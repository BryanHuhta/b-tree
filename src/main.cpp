#include <exception>
#include <iostream>

#include "BManager.h"
#include "global.h"
#include "Header.h"

int main(void)
{
    Header h;
    std::string temp = "2!0!00!11!22!";

    std::cout << h.ToString() << std::endl;

    h.FromString(temp);

    std::cout << h.ToString() << std::endl;

    return (0);
}
